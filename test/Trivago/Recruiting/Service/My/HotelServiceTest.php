<?php

class HotelServiceTest extends \TrivagoTest\TrivagoTestCase
{
    public function testGetHotelsForCity()
    {
        $hotelsRepository = new \Trivago\Recruiting\DataSource\Repository\HotelsRepository($this->getDataSource());
        $hotelService = new \Trivago\Recruiting\Service\My\HotelService(new \Trivago\Recruiting\Service\My\PartnerService($hotelsRepository), $hotelsRepository);
        $hotels = $hotelService->getHotelsForCity('Düsseldorf');

        $this->assertEquals(2, count($hotels));
        $this->assertEquals($hotels[0]->sName, 'Hilton Düsseldorf');
        $this->assertEquals($hotels[1]->aPartners[0]->sName, 'Booking.com');
    }
}
