<?php
/*
 * Well, fk. I have no idea where should I put this class.
 * Looks like this is the best place.
 */

namespace Trivago\Recruiting;

class EntityMapper
{
    /**
     * Map stdClass to entities
     * @see HotelService::exportRecordToHotelEntity
     * @param \stdClass $oSource
     * @param array $aConfiguration
     * @return array
     */
    public function map($oSource, $aConfiguration)
    {
        $entityClassName = $aConfiguration['entity'];
        $type = $aConfiguration['type'];
        $fields = $aConfiguration['fields'];

        if ($type == 'one') { // convert "one" record to "many" like
            $oSource = array($oSource);
        }

        $result = array();

        foreach($oSource as $record) {
            $entity = new $entityClassName();

            foreach($fields as $fieldNameSource => $fieldNameDestination) {
                if (is_array($subConfig = $fieldNameDestination)) {
                    $entity->{$subConfig['parent']} = $this->map($record->{$fieldNameSource}, $subConfig);
                } else {
                    $entity->{$fieldNameDestination} = $record->{$fieldNameSource};
                }
            }

            $result[] = $entity;
        }

        if ($type == 'one') {
            return $result[0];
        } else {
            return $result;
        }
    }
}


