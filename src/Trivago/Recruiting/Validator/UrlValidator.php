<?php
namespace Trivago\Recruiting\Validator;

class UrlValidator implements ValidatorInterface
{
    /**
     * Returns true if value if valid
     * This validator is used in Trivago\Recruiting\Entity\Hotel::__set method
     *
     * @param mixed $value
     * @return bool
     */
    public function isValid($value)
    {
        return filter_var($value, FILTER_VALIDATE_URL) !== false;
    }
}