<?php
namespace Trivago\Recruiting\DataSource\Filter;

use Trivago\Recruiting\DataSource\Adapter\ResultSetInterface;

interface FilterInterface
{
    /**
     * Returns filtered result set
     * @param ResultSetInterface $resultSet
     * @return ResultSetInterface
     */
    public function filter(ResultSetInterface $resultSet);
}