<?php
require __DIR__  . '/../src/SplClassLoader.php';

$oClassLoader = new \SplClassLoader('Trivago', __DIR__ . '/../src');
$oClassLoader->register();

require_once __DIR__.'/Trivago/TrivagoTestCase.php';

date_default_timezone_set('UTC');

define('TEST_JSON_DATA_SOURCE_DIR', __DIR__.'/data');
define('TEST_JSON_DATA_SOURCE_CITY_ID', 15475);