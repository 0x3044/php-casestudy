<?php
namespace Trivago\Recruiting\Entity;
use Trivago\Recruiting\Validator\UrlValidator;

/**
 * Represents a single partner from a search result.
 * 
 * @author mmueller
 */
class Partner
{
    /**
     * Name of the partner
     * @var string
     */
    public $sName;

    /**
     * Url of the partner's homepage (root link)
     * 
     * @var string
     */
    public $sHomepage;

    /**
     * Unsorted list of prices received from the 
     * actual search query.
     * 
     * @var Price[]
     */
    public $aPrices = array();

    /**
     * @param $name
     * @param $value
     * @throws \InvalidArgumentException
     */
    public function __set($name, $value)
    {
        switch($name) {
            default:
                $this->{$name} = $value;
                break;

            case 'sHomepage':
                $urlValidator = new UrlValidator();

                if(!($urlValidator->isValid($value))) {
                    throw new \InvalidArgumentException('Invalid URL');
                }

                $this->{$name} = $value;
                break;
        }
    }
}