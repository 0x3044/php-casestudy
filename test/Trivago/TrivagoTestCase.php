<?php
namespace TrivagoTest;

use Trivago\Recruiting\DataSource\Adapter\Json\Adapter;

class TrivagoTestCase extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->_dataSource = new Adapter(TEST_JSON_DATA_SOURCE_DIR);
    }

    /**
     * @var Adapter
     */
    protected $_dataSource;

    /**
     * @return \Trivago\Recruiting\DataSource\Adapter\Json\Adapter
     */
    protected function getDataSource()
    {
        return $this->_dataSource;
    }
}