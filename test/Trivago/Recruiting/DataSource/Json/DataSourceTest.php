<?php

class DataSourceTest extends TrivagoTest\TrivagoTestCase
{
    public function testFind()
    {
        $source = $this->_dataSource;
        $result = $source->find('city', TEST_JSON_DATA_SOURCE_CITY_ID);

        $this->assertTrue($result instanceof \stdClass);
        $this->assertEquals(TEST_JSON_DATA_SOURCE_CITY_ID, $result->id);
        $this->assertTrue(count((array)$result->hotels) == 2);
    }

    public function testFetchAll()
    {
        $source = $this->_dataSource;
        $resultSet = $source->fetchAll('city');

        $this->assertTrue($resultSet instanceof \Trivago\Recruiting\DataSource\Adapter\Json\ResultSet);
    }
}