<?php
namespace Trivago\Recruiting\Service;

use Trivago\Recruiting\DataSource\Repository\HotelsRepository;
use Trivago\Recruiting\Service\My\HotelService;

class HotelServiceFactory
{
    /**
     * @var PartnerServiceInterface
     */
    protected $partnerService;

    /**
     * @var HotelsRepository
     */
    protected $hotelsRepository;

    /**
     * Abstract Factory
     * @param PartnerServiceInterface $partnerService
     * @param HotelsRepository $hotelsRepository
     */
    public function __construct(PartnerServiceInterface $partnerService, HotelsRepository $hotelsRepository)
    {
        $this->partnerService = $partnerService;
        $this->hotelsRepository = $hotelsRepository;
    }

    /**
     * Returns My\HotelService
     * @return HotelService
     */
    public function createMyHotelService()
    {
        return new HotelService($this->partnerService, $this->hotelsRepository);
    }

    /**
     * Returns UnorderedHotelService
     * @return UnorderedHotelService
     */
    public function createUnorderedHotelService()
    {
        return new UnorderedHotelService($this->partnerService);
    }
}