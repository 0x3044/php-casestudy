<?php
namespace Trivago\Recruiting\DataSource\Adapter;

interface ResultSetInterface extends \Iterator, \Countable
{
    /**
     * RecordSet using simple stdClass objects
     * @param \stdClass[] $aRecords
     */
    public function __construct(array $aRecords);

    /**
     * Returns records
     * @return \stdClass[]
     */
    public function getRecords();
}