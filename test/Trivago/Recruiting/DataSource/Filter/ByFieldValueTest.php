<?php

class ByFieldValueTest extends TrivagoTest\TrivagoTestCase
{
    public function testFilterByCityName()
    {
        $records = $this->getDataSource()->fetchAll('city');
        $filterHasCity = new Trivago\Recruiting\DataSource\Filter\ByFieldValue('city', 'Düsseldorf');
        $filterHasNoCity = new Trivago\Recruiting\DataSource\Filter\ByFieldValue('city', '<some unexistsing city>');

        $this->assertEquals(1, count($records));
        $this->assertEquals(1, count($filterHasCity->filter($records)));
        $this->assertEquals(0, count($filterHasNoCity->filter($records)));
    }
}
 