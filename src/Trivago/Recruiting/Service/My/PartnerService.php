<?php
namespace Trivago\Recruiting\Service\My;

use Trivago\Recruiting\Service\PartnerServiceInterface;
use Trivago\Recruiting\DataSource\Repository\HotelsRepository;

class PartnerService implements PartnerServiceInterface
{
    /**
     * @var HotelsRepository
     */
    protected $hotelsRepository;

    /**
     * Your constructor just wasn't described in your interface.
     * Huh! I can use DI here! :)
     *
     * @param \Trivago\Recruiting\DataSource\Repository\HotelsRepository $hotelsRepository
     * @internal param \Trivago\Recruiting\DataSource\Repository\HotelsRepository $cityDataSource
     */
    public function __construct(HotelsRepository $hotelsRepository)
    {
        $this->hotelsRepository = $hotelsRepository;
    }

    /**
     * This method should read from a datasource (JSON in our case)
     * and return an unsorted list of hotels found in the datasource.
     *
     * @param integer $iCityId
     *
     * @return \Trivago\Recruiting\Entity\Hotel[]
     */
    public function getResultForCityId($iCityId)
    {
        return $this->hotelsRepository->getHotelsForCityId($iCityId);
    }
}