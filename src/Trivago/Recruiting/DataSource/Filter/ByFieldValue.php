<?php
namespace Trivago\Recruiting\DataSource\Filter;

use Trivago\Recruiting\DataSource\Adapter\Json\ResultSet;
use Trivago\Recruiting\DataSource\Adapter\ResultSetInterface;

final class ByFieldValue implements FilterInterface
{
    protected $sFieldName;
    protected $value;

    /**
     * Filter resultset by field's value
     * @param string $sFieldName
     * @param mixed $value
     */
    public function __construct($sFieldName, $value)
    {
        $this->sFieldName = $sFieldName;
        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     * @param ResultSetInterface $resultSet
     * @return ResultSet
     */
    public function filter(ResultSetInterface $resultSet)
    {
        $result = array();
        $fieldName = $this->sFieldName;
        $value = $this->value;

        /** @var $record ResultSetInterface */
        foreach($resultSet as $record) {
            if (isset($record->{$fieldName}) && ($record->{$fieldName} === $value)) {
                $result[] = $record;
            }
        }

        return new ResultSet($result);
    }
}