<?php
namespace Trivago\Recruiting\DataSource\Repository;

use Trivago\Recruiting\DataSource\Adapter\AdapterInterface;
use Trivago\Recruiting\DataSource\Adapter\ResultSetInterface;
use Trivago\Recruiting\EntityMapper;
use Trivago\Recruiting\Entity\Hotel;

class HotelsRepository
{
    /**
     * @var AdapterInterface
     */
    protected $adapter;

    /**
     * CityDataSource
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * Returns list of hotels by cityName
     * @param string $sCityName
     * @return array
     */
    public function getHotelsForCityName($sCityName)
    {
        return $this->exportResultSetToHotelEntities($this->adapter->findBy('city', 'city', $sCityName));
    }

    /**
     * Returns ids for cities with given name
     * Its pretty slow way for "production" but idc
     * @param $sCityName
     * @return array
     */
    public function getCityIdsByCityName($sCityName)
    {
        $ids = array();

        foreach($this->adapter->fetchAll('city') as $city) {
            $ids[(string) $city->id] = true;
        }

        return array_keys($ids);
    }

    /**
     * Returns list of hotels by cityId
     * @param int $iCityId
     * @return \Trivago\Recruiting\Entity\Hotel[]
     */
    public function getHotelsForCityId($iCityId)
    {
        return $this->exportResultSetToHotelEntities($this->adapter->findBy('city', 'id', $iCityId));
    }

    /**
     * Export resultSet to entities
     *
     * @param ResultSetInterface $resultSet
     * @return array
     */
    protected function exportResultSetToHotelEntities(ResultSetInterface $resultSet)
    {
        $hotels = array();

        /** @var $oRecord \stdClass */
        foreach($resultSet as $oRecord) {
            foreach($oRecord->hotels as $oHotel) {
                $hotels[] = $this->exportRecordToHotelEntity($oHotel);
            }
        }

        return $hotels;
    }

    /**
     * Export stdClass record to hotel entity
     *
     * @param \stdClass $oHotel
     * @return Hotel[]
     */
    protected function exportRecordToHotelEntity($oHotel)
    {
        $mapper = new EntityMapper();
        $mapperConfiguration = array(
            'entity' => '\Trivago\Recruiting\Entity\Hotel',
            'type' => 'one',
            'fields' => array(
                'name' => 'sName',
                'adr' => 'sAdr',
                'partners' => array(
                    'entity' => '\Trivago\Recruiting\Entity\Partner',
                    'type' => 'many',
                    'parent' => 'aPartners',
                    'fields' => array(
                        'name' => 'sName',
                        'url' => 'sHomepage',
                        'prices' => array(
                            'entity' => '\Trivago\Recruiting\Entity\Price',
                            'type' => 'many',
                            'parent' => 'aPrices',
                            'fields' => array(
                                'description' => 'sDescription',
                                'amount' => 'sAmount',
                                'from' => 'oFromDate',
                                'to' => 'oToDate'
                            )
                        )
                    )
                )
            )
        );

        return $mapper->map($oHotel, $mapperConfiguration);
    }
}