<?php
namespace Trivago\Recruiting\DataSource\Adapter\Json;

use Trivago\Recruiting\DataSource\Adapter\AdapterInterface;
use Trivago\Recruiting\DataSource\Filter\ByFieldValue;
use Trivago\Recruiting\DataSource\Adapter\ResultSetInterface;

class Adapter implements AdapterInterface
{
    /**
     * Directory with JSON-files
     * @var string
     */
    protected $sResourceDir;

    /**
     * JSON DataSource via FS
     * @param $sResourceDir
     * @throws \Exception
     * @internal param $collectionName
     */
    public function __construct($sResourceDir)
    {
        if (!(file_exists($sResourceDir) && is_dir($sResourceDir) && is_readable($sResourceDir))) {
            throw new \Exception(sprintf('Path [%s] is not readable or not a valid directory', $sResourceDir));
        }

        $this->sResourceDir = $sResourceDir;
    }

    /**
     * Return dir which contains records as json files
     * @return string
     */
    private function getResourceDir()
    {
        return $this->sResourceDir;
    }

    /**
     * {@inheritdoc}
     * @param string $sCollectionName
     * @param int $iId
     * @return \stdClass
     * @throws \OutOfBoundsException
     * @throws \InvalidArgumentException
     */
    public function find($sCollectionName, $iId)
    {
        if (!(is_int($iId))) {
            throw new \InvalidArgumentException(sprintf('Invalid id %s', var_export($iId, true)));
        }

        if (!($this->validateCollectionName($sCollectionName))) {
            throw new \InvalidArgumentException(sprintf('Invalid collection %s', var_export($sCollectionName, true)));
        }

        $jsonFilePath = $this->getResourceDir() . '/' . $sCollectionName . '/' . $iId . '.json';

        if (($fileContent = file_get_contents($jsonFilePath)) === false) {
            throw new \OutOfBoundsException(sprintf('Record #%d not found'), (int)$iId);
        }

        return json_decode($fileContent, false);
    }

    /**
     * {@inheritdoc}
     * @param string $sFieldName
     * @param string $value
     * @return ResultSetInterface
     */
    public function findBy($sCollectionName, $sFieldName, $value)
    {
        $filter = new ByFieldValue($sFieldName, $value);

        if (!($this->validateCollectionName($sCollectionName))) {
            throw new \InvalidArgumentException(sprintf('Invalid collection %s', var_export($sCollectionName, true)));
        }

        return $filter->filter($this->fetchAll($sCollectionName));
    }

    /**
     * {@inheritdoc}
     * @return ResultSet
     * @throws \OutOfBoundsException
     */
    public function fetchAll($sCollectionName)
    {
        if (!($this->validateCollectionName($sCollectionName))) {
            throw new \InvalidArgumentException(sprintf('Invalid collection %s', var_export($sCollectionName, true)));
        }

        $records = array();
        $dirHandler = opendir($this->getResourceDir() . '/' . $sCollectionName);

        while ($fileName = readdir($dirHandler)) {
            $path = $this->getResourceDir() . '/' . $sCollectionName . '/' . $fileName;

            if (is_file($path) && preg_match_all('/^(?P<id>\d+)\.json$/', $fileName, $pregMatch)) {
                if (($fileContent = file_get_contents($path)) === false) {
                    throw new \OutOfBoundsException(sprintf('Failed to read file [%s]'), $fileName);
                }

                $records[] = json_decode($fileContent, false);
            }
        }

        return new ResultSet($records);
    }

    /**
     * Returns true if input is a valid collection name
     * @param string $sCollectionName
     * @return bool
     */
    protected function validateCollectionName($sCollectionName)
    {
        return is_string($sCollectionName) && strlen($sCollectionName) > 0 && !(preg_match('/[^a-zA-Z\_\-]/', $sCollectionName));
    }
}