<?php
namespace Trivago\Recruiting\DataSource\Adapter;

interface AdapterInterface
{
    /**
     * Find record by id
     * @param $sCollectionName
     * @param $iId
     * @return \stdClass
     */
    public function find($sCollectionName, $iId);

    /**
     * Returns records by field's value
     * @param $sCollectionName
     * @param $sFieldName
     * @param $value
     * @return ResultSetInterface
     */
    public function findBy($sCollectionName, $sFieldName, $value);

    /**
     * Returns all records
     * @param $sCollectionName
     * @return ResultSetInterface
     */
    public function fetchAll($sCollectionName);
}