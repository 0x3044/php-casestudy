<?php
namespace Trivago\Recruiting\Validator;

interface ValidatorInterface
{
    /**
     * {@inheritdoc}
     *
     * @param mixed $value
     * @return bool
     */
    public function isValid($value);
}