<?php
namespace Trivago\Recruiting\Service\My;

use Trivago\Recruiting\DataSource\Repository\HotelsRepository;
use Trivago\Recruiting\Service\HotelServiceInterface;
use Trivago\Recruiting\Service\PartnerServiceInterface;

class HotelService implements HotelServiceInterface
{
    /**
     * @var PartnerServiceInterface
     */
    protected $partnerService;

    /**
     * @var HotelsRepository
     */
    protected $hotelsRepository;

    public function __construct(PartnerServiceInterface $partnerService, HotelsRepository $hotelsRepository)
    {
        $this->partnerService = $partnerService;
        $this->hotelsRepository = $hotelsRepository;
    }

    /**
     * @param string $sCityName Name of the city to search for.
     *
     * @throws \Exception
     * @return \Trivago\Recruiting\Entity\Hotel[]
     */
    public function getHotelsForCity($sCityName)
    {
        $cityIds = $this->hotelsRepository->getCityIdsByCityName($sCityName);
        $hotels = array();

        foreach($cityIds as $cityId) {
            $hotels = array_merge($hotels, $this->partnerService->getResultForCityId($cityId));
        }

        return $hotels;
    }
}